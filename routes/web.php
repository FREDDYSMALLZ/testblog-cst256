<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/hello', 'HomeController@hello');

Route::get('/helloworld', 'HomeController@helloworld');

Route::get('/test', 'TestController@test');

Route::get('/test2', 'TestController@test2');

//Route::get('/whoami','WhatsMyNameController@index');

Route::post('/whoami','WhatsMyNameController@index');

Route::get('/askme', function () {

    return view('whoami'); });


//Route for Login
Route::get('/login', function ()
{
    return view('login');
});

Route::post('/dologin', 'LoginController@index');


//New Route for Login 2
Route::get('/login2', function ()
{
    return view('login2');
});

//New Route for login3
Route::get('/login3', function ()
{
    return view('login3');
});

//New Route for Login 3 at index
Route::post('/dologin3', 'Login3Controller@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
