@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        Hello World... This is a new Route that Renders a Text response
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
