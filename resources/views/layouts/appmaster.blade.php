<html lange="en">
<head>
    <title>@yield('Login 2')</title>
</head>

<body>
@include('layouts.header')
<div align="center">
    @yield('content')
</div>
@include('layouts.footer')
</body>

</html>