<?php

namespace App\Models;

class UserModel
{
    //user variables
    private $id;
    private $username;
    private $password;

   //Getter Methods
    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

     //UserModel constructor.
    public function __construct($id, $username, $password)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
    }

}