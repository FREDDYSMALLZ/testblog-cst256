<?php

namespace App\Http\Controllers;

use App\Models\UserModel;
use App\Services\Business\SecurityService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    // index function
    public function index(Request $request)
    {
        try {
            $username = $request->input('username');
            $password = $request->input('password');

            $user = new UserModel(- 1, $username, $password);

            $service = new SecurityService();
            $status = $service->login($user);

            if ($status) {
                $data = [
                    'model' => $user
                ];
                return view('loginPassed2')->with($data);
            }
            else {
                return view('loginFailed2');
            }
        }
        catch(Exception $e)
        {
            Log::error("Exception: ", array("message" => $e->getMessage()));
            $data = ['errorMsg' => $e->getMessage()];
            return view("systemException")->with($data);

        }

        Log::info("Entering LoginController::index()");

        Log::info("Parameters are: ", array("username" => $username, "password" => $password));

        Log::info("Exit LoginController::index() with login passing");
    }
}
