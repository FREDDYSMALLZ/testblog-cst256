<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
class TestController extends Controller
{
    /**
     * Test controller with signature of public function test() that returns
     * a text response from the test controller
     */
    public function test()
    {
        return "Hello World... This is a new Route that Renders a Text response";
    }

    public function test2()
    {
        return view('helloworld');
    }
}
