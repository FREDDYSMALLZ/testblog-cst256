<?php

namespace App\Http\Controllers;

use App\Models\UserModel;
use App\Services\Business\SecurityService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Login3Controller extends Controller
{
    // index function
    public function index(Request $request)
    {
        try {
            $username = $request->input('username');
            $password = $request->input('password');

            $user = new UserModel(-1, $username, $password);

            $service = new SecurityService();
            $status = $service->login($user);

            if ($status) {
                $data = [
                    'model' => $user
                ];
                return view('loginPassed2')->with($data);
            } else {
                return view('loginFailed2');
            }
        } catch (Exception $e) {
            Log::error("Exception: ", array("message" => $e->getMessage()));
            $data = ['errorMsg' => $e->getMessage()];
            return view("systemException")->with($data);
        }
    }

    private function validateForm(Request $request)
    {
        // Setup Data Validation Rules for Login Form
        $rules = ['username' => 'Required | Between:4,10 | Alpha',
            'password' => 'Required | Between:4,10'];

        // Run Data Validation Rules
        $this->validate($request, $rules);
    }

}
